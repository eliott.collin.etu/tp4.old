import Router from '../Router.js';
import Page from './Page.js';
import PizzaList from './PizzaList.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
					Image :<br/>
					<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}



	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit() {
		console.log(this.element.querySelector('input[name="name"]'));
		console.log(this.element.querySelector('input[name="image"]'));
		console.log(this.element.querySelector('input[name="price_small"]'));
		console.log(this.element.querySelector('input[name="price_large"]'));
		
		
		// D.4. La validation de la saisie
		const name = this.element.querySelector('input[name="name"]').value,
			image = this.element.querySelector('input[name="image"]').value,
			price_small = this.element.querySelector('input[name="price_small"]').value,
			price_large = this.element.querySelector('input[name="price_large"]').value;
			
		if (name === '') {
			alert('Erreur : le champ "Nom" est obligatoire');
			return;
		}
		else if (image === '') {
			alert('Erreur : le champ "image" est obligatoire');
			return;
		} else if (price_small === '') {
			alert('Erreur : le champ "prix petit format" est obligatoire');
			return;
		} else if (price_large === '') {
			alert('Erreur : le champ "prix grand format" est obligatoire');
			return;
		} else {
		}	
		
		let pizza = {
			name : name,
			image : image,
			price_small : Number(price_small),
			price_large : Number(price_large)
		};
		
		alert(`La pizza ${name} a été ajoutée !`);
		// name = '';
		// image = '';
		// price_small = '';
		// price_large = '';

		fetch(
			'http://localhost:8080/api/v1/pizzas',
			{
				method:'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(pizza)
			}
		)
		.then(() => Router.navigate('/'));
		
	}
}
